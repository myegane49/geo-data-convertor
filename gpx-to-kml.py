from xml.dom import minidom

doc = minidom.Document()
kml = doc.createElement('kml') 
kml.setAttribute('xmlns', 'http://www.opengis.net/kml/2.2')
kml.setAttribute('xmlns:gx', 'http://www.google.com/kml/ext/2.2')
doc.appendChild(kml)

Document = doc.createElement('Document')
name0 = doc.createElement('name')
name0_text = doc.createTextNode('Bookmarks')
name0.appendChild(name0_text)
Document.appendChild(name0)

Folder = doc.createElement('Folder')
name1 = doc.createElement('name')
name1_text = doc.createTextNode('Default')
name1.appendChild(name1_text)
Folder.appendChild(name1)

tree = minidom.parse('./from-json.gpx')
for wpt in tree.getElementsByTagName('wpt'):
    Placemark = doc.createElement('Placemark')
    name2 = doc.createElement('name')
    name2_text = doc.createTextNode(wpt.getElementsByTagName('name')[0].firstChild.data)
    name2.appendChild(name2_text)
    Placemark.appendChild(name2)

    description = doc.createElement('description')
    description_text = doc.createTextNode(wpt.getElementsByTagName('desc')[0].firstChild.data)
    description.appendChild(description_text)
    Placemark.appendChild(description)

    LookAt = doc.createElement('LookAt')
    longitude = doc.createElement('longitude')
    longitude_text = doc.createTextNode(wpt.getAttribute('lon'))
    longitude.appendChild(longitude_text)
    LookAt.appendChild(longitude)

    latitude = doc.createElement('latitude')
    latitude_text = doc.createTextNode(wpt.getAttribute('lat'))
    latitude.appendChild(latitude_text)
    LookAt.appendChild(latitude)
    Placemark.appendChild(LookAt)

    Style = doc.createElement('Style')
    IconStyle = doc.createElement('IconStyle')
    Icon = doc.createElement('Icon')
    href = doc.createElement('href')
    href_text = doc.createTextNode('/usr/share/marble/data/bitmaps/bookmark.png')
    href.appendChild(href_text)
    Icon.appendChild(href)
    IconStyle.appendChild(Icon)
    Style.appendChild(IconStyle)
    Placemark.appendChild(Style)

    Point = doc.createElement('Point')
    coordinates = doc.createElement('coordinates')
    coordinates_text = doc.createTextNode(wpt.getAttribute('lon') + ',' + wpt.getAttribute('lat'))
    coordinates.appendChild(coordinates_text)
    Point.appendChild(coordinates)
    Placemark.appendChild(Point)

    ExtendedData = doc.createElement('ExtendedData')
    Data = doc.createElement('Data')
    Data.setAttribute('name', 'isBookmark')
    value = doc.createElement('value')
    value_text = doc.createTextNode('true')
    value.appendChild(value_text)
    Data.appendChild(value)
    ExtendedData.appendChild(Data)
    Placemark.appendChild(ExtendedData)

    Folder.appendChild(Placemark)

Document.appendChild(Folder)
kml.appendChild(Document)

xml_str = doc.toprettyxml(indent ="\t", encoding='UTF-8')
with open('from-gpx.kml', "wb") as f:
    f.write(xml_str)
