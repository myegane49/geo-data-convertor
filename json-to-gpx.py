from json import load
from xml.dom import minidom

doc = minidom.Document()
gpx = doc.createElement('gpx') 
gpx.setAttribute('version', '1.1')
gpx.setAttribute('creator', 'OsmAnd 4.2.5')
gpx.setAttribute('xmlns', 'http://www.topografix.com/GPX/1/1')
gpx.setAttribute('xmlns:osmand', 'https://osmand.net')
gpx.setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
gpx.setAttribute('xsi:schemaLocation', 'http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd')
doc.appendChild(gpx)

metadata = doc.createElement('metadata')
gpx.appendChild(metadata)

name = doc.createElement('name')
name_text = doc.createTextNode('')
name.appendChild(name_text)
metadata.appendChild(name)

time = doc.createElement('time')
time_text = doc.createTextNode('2022-07-02T08:44:45Z')
time.appendChild(time_text)
metadata.appendChild(time)


points = []
with open('data.json', mode='r') as file:
    data = load(file)
    points = data['features']

for point in points:
    point_data = point['properties']
    wpt = doc.createElement('wpt')
    wpt.setAttribute('lat', point_data['Location']['Geo Coordinates']['Latitude'])
    wpt.setAttribute('lon', point_data['Location']['Geo Coordinates']['Longitude'])

    time = doc.createElement('time')
    time_text = doc.createTextNode(point_data['Published'])
    time.appendChild(time_text)
    wpt.appendChild(time)

    name = doc.createElement('name')
    name_text = doc.createTextNode(point_data['Title'])
    name.appendChild(name_text)
    wpt.appendChild(name)

    desc = doc.createElement('desc')
    try:
        desc_text = doc.createTextNode(point_data['Location']['Business Name'])
    except KeyError:
        desc_text = doc.createTextNode('no description')
    desc.appendChild(desc_text)
    wpt.appendChild(desc)

    extensions = doc.createElement('extensions')
    address = doc.createElement('osmand:address')
    try:
        address_text = doc.createTextNode(point_data['Location']['Address'])
    except KeyError:
        address_text = doc.createTextNode('no address')
    address.appendChild(address_text)
    extensions.appendChild(address)

    icon = doc.createElement('osmand:icon')
    icon_text = doc.createTextNode('park')
    icon.appendChild(icon_text)
    extensions.appendChild(icon)

    background = doc.createElement('osmand:background')
    background_text = doc.createTextNode('circle')
    background.appendChild(background_text)
    extensions.appendChild(background)

    color = doc.createElement('osmand:color')
    color_text = doc.createTextNode('#eecc22')
    color.appendChild(color_text)
    extensions.appendChild(color)

    wpt.appendChild(extensions)

    gpx.appendChild(wpt)

extensions = doc.createElement('extensions')
points_groups = doc.createElement('osmand:points_groups')
group = doc.createElement('group')
group.setAttribute('name', '')
group.setAttribute('color', '#eecc22')
group.setAttribute('icon', 'park')
group.setAttribute('background', 'circle')
points_groups.appendChild(group)
extensions.appendChild(points_groups)
gpx.appendChild(extensions)

xml_str = doc.toprettyxml(indent ="\t", encoding='UTF-8', standalone=True)
with open('from-json.gpx', "wb") as f:
    f.write(xml_str)

