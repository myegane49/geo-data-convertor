### About
A python script to transform your geoJSON data that you export from Google Maps to a .gpx file so that you can import it in your [Osmand](https://osmand.net/docs/versions/free-versions) Android app. And from .gpx to .kml file so that you can import it in [KDE Marble](https://apps.kde.org/marble/) on your Linux machine. Good luck on getting rid of Google for good.
### Dependencies
The dependencies are just the [python](https://www.python.org/) itself that needs to be installed on your computer
### Usage
to use it you need to name your .json file **data.json** and place it in the same directory as the scripts. Notice that all your points in Google Map needs to be stared points. Other point types won't be transformed. and you get a from-json.gpx file by running:<br>
`python json-to-gpx.py`<br>
To convert your gpx to kml run:<br>
`python gpx-to-kml.py`<br>
And you'll get a from-gpx.kml.
